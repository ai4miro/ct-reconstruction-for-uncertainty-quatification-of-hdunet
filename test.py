from monai.inferers import sliding_window_inference
from monai.data import CacheDataset, DataLoader, decollate_batch
from monai.transforms import (
    EnsureChannelFirstd,
    Compose,
    Orientationd,
    ScaleIntensityRanged,
    ToTensord,
    ConcatItemsd
)
import torch
import os, argparse
import json
from torch.nn import MaxPool3d, ReLU
from modular_hdunet import modular_hdunet
from custom_transforms import Separate_structsd, LoadImaged,SaveImaged
import torch.multiprocessing 
torch.multiprocessing.set_sharing_strategy('file_system')


parser = argparse.ArgumentParser(description="HDUNet for dose prediction pipeline")
parser.add_argument("--json_list", default="dataset_0.json", type=str, help="dataset json file")
parser.add_argument("--infer_overlap", default=0.6, type=float, help="sliding window inference overlap")
parser.add_argument("--in_channels", default=4, type=int, help="number of input channels")
parser.add_argument("--batch_size", default=1, type=int, help="number of batch size")
parser.add_argument("--a_min", default=-1000, type=float, help="a_min in ScaleIntensityRanged")
parser.add_argument("--a_max", default=1560, type=float, help="a_max in ScaleIntensityRanged")
parser.add_argument("--b_min", default=0.0, type=float, help="b_min in ScaleIntensityRanged")
parser.add_argument("--b_max", default=1.0, type=float, help="b_max in ScaleIntensityRanged")
parser.add_argument("--space_x", default=1.5, type=float, help="spacing in x direction")
parser.add_argument("--space_y", default=1.5, type=float, help="spacing in y direction")
parser.add_argument("--space_z", default=2.0, type=float, help="spacing in z direction")
parser.add_argument("--roi_x", default=128, type=int, help="roi size in x direction")
parser.add_argument("--roi_y", default=128, type=int, help="roi size in y direction")
parser.add_argument("--roi_z", default=128, type=int, help="roi size in z direction")
parser.add_argument("--workers", default=8, type=int, help="number of workers")
parser.add_argument("--output_dir", default="./outputs/", type=str, help="output directory")
parser.add_argument("--logdir", default="./log_128/", type=str, help="directory where the trained network is stored")

def main():
    args = parser.parse_args()
    
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    # Load Json & Append Root Path
    with open(args.json_list, 'r') as json_f:
        json_Data = json.load(json_f)
    data = json_Data['folds']
    out_Data = json_Data['out']
    nfolds = json_Data['nfolds']


    test_transforms = Compose(
        [
            LoadImaged(keys=["patient_name","image", "oar_together", "tv_together","prior_knowledge","dose"]),
            EnsureChannelFirstd(keys=["image", "oar_together", "tv_together","prior_knowledge", "dose"]),
            ScaleIntensityRanged(
                keys=["image"], a_min=args.a_min, a_max=args.a_max,
                b_min=args.b_min, b_max=args.b_max, clip=True,
            ),
            Orientationd(keys=["image", "prior_knowledge", "tv_together", "oar_together", "dose"], axcodes="RAS"),
            Separate_structsd(keys=["oar_together"]),
            ToTensord(keys=["oar_split"]),
            ConcatItemsd(keys=['prior_knowledge','image',"tv_together", "oar_split"],name="input",dim=0),
            ToTensord(keys=["input","image","prior_knowledge","tv_together","dose"])
        ]
    )
    test_posttransforms = Compose(
        [
            SaveImaged(keys="pred_dose", meta_keys="image_meta_dict", output_postfix="dose",separate_folder=False, output_dir=args.output_dir, resample=False),
            SaveImaged(keys="pred_ct", meta_keys="image_meta_dict", output_postfix="ct",separate_folder=False, output_dir=args.output_dir, resample=False)
        ]
    )

    device = torch.device("cuda:0")
    model_param = dict(
        base_num_filter=args.in_channels,
        num_blocks_per_stage_encoder=2,
        num_stages=4,
        pool_kernel_sizes=((2, 2, 2), (2, 2, 2), (2, 2, 2), (2, 2, 2)),
        conv_kernel_sizes=((3, 3, 3), (3, 3, 3), (3, 3, 3), (3, 3, 3)),
        conv_bottleneck_kernel_sizes=((3, 3, 3), (3, 3, 3), (3, 3, 3), (3, 3, 3)),
        num_blocks_per_stage_decoder=None,
        padding='same',
        num_steps_bottleneck=4,
        expansion_rate=1,
        pooling_type=MaxPool3d,
        pooling_kernel_size=(2, 2, 2),
        nonlin=ReLU
    )
    model = modular_hdunet(**model_param).to(device)

    d_path = {
        "description": "han_3d_seg_prediction_flexibleUNET",
        "modality": {
            "0": "CT"
        },
        "name": "han_3d_seg",
        "numTest": 6,
        "numTraining": 47,
        "tensorImageSize": "3D",
        "nfolds":nfolds,
        'test':[],
        "out":[]}
    out_ds = CacheDataset(data=out_Data, transform=test_transforms)
    out_loader = DataLoader(out_ds, batch_size=1, shuffle=True, num_workers=1)

    for f in range(nfolds):
        model_path = os.path.join(args.logdir,"best_metric_model_{}.pth".format(f))
        out_posttransforms = Compose(
        [
            SaveImaged(keys="pred_dose", meta_keys="image_meta_dict", output_postfix="dose_{}".format(f),separate_folder=False, output_dir=args.output_dir, resample=False),
            SaveImaged(keys="pred_ct", meta_keys="image_meta_dict", output_postfix="ct_{}".format(f),separate_folder=False, output_dir=args.output_dir, resample=False)
        ]
        )
        trained_model_dict = torch.load(model_path)
        model.load_state_dict(trained_model_dict['state_dict'])
        model = model.to(device)

        model.eval()

        test_Data = data[f]['test']
        # Define DataLoader using MONAI, CacheDataset needs to be used
        test_ds = CacheDataset(data=test_Data, transform=test_transforms)
        test_loader = DataLoader(test_ds, batch_size=1, shuffle=True, num_workers=4)

        with torch.no_grad():
        
            for i, d in enumerate(out_loader):
                
                images = d["input"].to(device)
                d['pred_dose'], d['pred_ct'] = sliding_window_inference(inputs=images, roi_size=(args.roi_x, args.roi_y, args.roi_z),sw_batch_size=1, predictor = model)
                if f == 0:
                    d_path['out'].append({"patient_name":d["patient_name"][0],"image":d["image_meta_dict"]['filename_or_obj'][0],"dose":d["dose_meta_dict"]['filename_or_obj'][0], "pred_ct":[], "pred_dose":[],"oar_together":d["oar_together_meta_dict"]['filename_or_obj'][0],"tv_together":d["tv_together_meta_dict"]['filename_or_obj'][0]})
                d_path['out'][i]['pred_ct'].append(os.path.join(args.output_dir,"{}_ct_{}.nii.gz".format(d['patient_name'][0],f)))
                d_path['out'][i]['pred_dose'].append(os.path.join(args.output_dir,"{}_dose_{}.nii.gz".format(d['patient_name'][0],f)))
                d['pred'] = [out_posttransforms(i) for i in decollate_batch(d)]

            for d in test_loader:
                print("d.keys()",d.keys())
                images = d["input"].to(device)
                d['pred_dose'], d['pred_ct'] = sliding_window_inference(inputs=images, roi_size=(args.roi_x, args.roi_y, args.roi_z),sw_batch_size=1, predictor = model)
                d_path['test'].append({ "patient_name":d["patient_name"][0],
                                        "image":d["image_meta_dict"]['filename_or_obj'][0],
                                        "dose":d["dose_meta_dict"]['filename_or_obj'][0], 
                                        "pred_ct":os.path.join(args.output_dir,d['patient_name'][0]+"_ct.nii.gz"), 
                                        "pred_dose":os.path.join(args.output_dir,d['patient_name'][0]+"_dose.nii.gz"),
                                        "oar_together":d["oar_together_meta_dict"]['filename_or_obj'][0],
                                        "tv_together":d["tv_together_meta_dict"]['filename_or_obj'][0],
                                        "fold":f})

                d['pred'] = [test_posttransforms(i) for i in decollate_batch(d)]

        print('d["image_meta_dict"]["filename_or_obj"]',d["image_meta_dict"]['filename_or_obj'])

    with open(os.path.join(args.output_dir,'json_dataset_file.json'), "w") as outfile:
        json.dump(d_path, outfile,indent=2)


if __name__ == "__main__":
    main()
