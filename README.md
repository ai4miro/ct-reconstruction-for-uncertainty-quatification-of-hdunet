# CT reconstruction for uncertainty quatification of HDUNet

**Background:** Estimating the uncertainty of deep learning (DL) models in a reliable and efficient way has remained an open research question, crucial to answer in medical applications, where DL is becoming an ubiquitous tool. In radiotherapy, a popular DL application is the prediction of optimal 3D dose distributions for a given patient. The predicted dose can later be used for treatment decision support and in automatic treatment planning. Thus, properly estimating model uncertainty in such critical environments is imperative, since predictions with large uncertainty can jeopardize patients safety.
Most common uncertainty estimation methods are based on Bayesian approximations, like Monte Carlo dropout (MCDO) or Deep ensembling (DE). These two techniques, however, have a high inference time (i.e. require multiple inference passes) and might not work for out-of-distribution (OOD) detection data (i.e. overlapping uncertainty estimate for in-distribution (ID) and OOD). In radiotherapy dose prediction, fast uncertainty quantification and efficient OOD detection is a must, especially as research is investigating adaptive treatments.

**Purpose:** In this study, we present a direct uncertainty estimation method and apply it for a dose prediction U-Net architecture. It can be used to flag OOD data and give information on the quality of the dose prediction.

**Methods** Our method consists in the addition of a branch decoding from the bottleneck which reconstructs the CT scan given as input. The input reconstruction error can be used as a surrogate of the model uncertainty. For the proof-of-concept, our method is applied to proton therapy dose prediction in head and neck cancer patients. A dataset of 60 oropharyngeal patients was used to train the network using a nested cross-validation approach with 11 folds (training: 50 patients, validation: 5 patients, test: 5 patients). For the OOD experiment, we also used 10 patients with nasopharyngeal and laryngeal cancers. Accuracy, time-gain, and OOD detection are analyzed for our method in this particular application and compared with the popular MCDO and DE.

**Results** The additional branch did not reduce the accuracy of the dose prediction model. The median absolute error is close to zero for the target volumes and less than 1\% for organs at risk. Our input reconstruction method showed a higher Pearson correlation coefficient with the prediction error (0.620) than DE (0.447) and MCDO (between 0.599 and 0.612). Moreover, our method allows an easier identification of OOD (no overlap for ID and OOD data and a Z-score of 34.05). The uncertainty is estimated simultaneously to the regression task, therefore requires less time or computational resources.

**Conclusions** This study shows that the error in the CT scan reconstruction can be used as a surrogate of the uncertainty of the model. The Pearson correlation coefficient with the dose prediction error is slightly higher than state-of-the-art techniques. OOD data can be more easily detected and the uncertainty metric is computed simultaneously to the regression task, therefore faster than MCDO or DE.

[Article on ArXiv](https://arxiv.org/pdf/2310.19686.pdf)
## Data format

We used the Nifti format to contain the data used in the training of the models in the following manner:
The nifti headers all contain information common to all files such as the voxelsize (pixdim) and arrayshape (dim).
- ct.nii.gz: contains the CT 3D matrix.
- struct_oar.nii.gz: contains the masks of organs at risks, one hot encoded. The header of the nifti contains an additional extension field containing the contours_exist list (1 if the corresponding OAR is present, 0 otherwise).
- struct_tv.nii.gz: contains the target volumes masks with prescription value encoded in the voxels.
- prior_knowledge.nii.gz: contains the dose distribution when applying the template in the treatment planning software without the manual tuning of the dosimetrists. 
- dose.nii.gz: contains the clinical dose distribution, the label for the training.

We trained on an oropharyngeal cancer database and included the following 13 organs at risk : ['BRAINSTEM', 'ESOPHAGUS_UPPER', 'GLOTTICAREA', 'ORALCAVITY', 'PAROTID_L', 'PAROTID_R', 'PHARCONSINF', 'PHARCONSMID', 'PHARCONSSUP', 'SMG_L', 'SMG_R', 'SPINALCORD', 'SUPRAGLOTLARYNX']
## Step 1 - Create json file

Run the create_json script to generate a json file with paths of all data.

## Step 2 - Launch training

```bash
python train.py --in_channels 16 --max_epochs 400 --val_every 10 --batch_size 1 --roi_x 128 --roi_y 128 --roi_z 128 --json_list json_dataset_file.json --logdir FOLDER_WHERE_PRETRAINED_MODELS_WILL_BE_STORED 
```

## Step 3 - Launch test

```bash
python test.py --in_channels 16 --logdir FOLDER_WHERE_PRETRAINED_MODELS_ARE_STORED --infer_overlap=0.5 --workers 4  --json_list json_dataset_file.json  --output_dir FOLDER_WHERE_PREDICTIONS_WILL_BE_STORED
```

## Step 4 - Launch evaluation script

```bash
python eval.py --output_dir FOLDER_WHERE_OUTPUTS_OF_EVALUATION_WILL_BE_STORED --workers 4 --json_list FOLDER_WHERE_PREDICTIONS_WILL_BE_STORED/json_dataset_file.json 
```
