from monai.transforms import (
    EnsureChannelFirstd,
    Compose,
    Orientationd,
    ScaleIntensityRanged,
    RandCropByPosNegLabeld, 
    ToTensord,
    ConcatItemsd,
    SpatialPadd

)
from custom_transforms import Separate_structsd, LoadImaged
from monai.data import CacheDataset, DataLoader

import json
import torch
import argparse
import os
from modular_hdunet import modular_hdunet
from torch.nn import MaxPool3d, ReLU
import torch.multiprocessing 
torch.multiprocessing.set_sharing_strategy('file_system')

parser = argparse.ArgumentParser(description="HDUNet for dose prediction pipeline")
parser.add_argument("--checkpoint", default=None, help="start training from saved checkpoint")
parser.add_argument("--logdir", default="test", type=str, help="directory to save the tensorboard logs")
parser.add_argument("--start_fold", default=0, type=int, help="data fold")
parser.add_argument("--pretrained_model_name", default="model.pt", type=str, help="pretrained model name")
parser.add_argument("--json_list", default="./jsons/brats21_folds.json", type=str, help="dataset json file")
parser.add_argument("--save_checkpoint", action="store_true", help="save checkpoint during training")
parser.add_argument("--max_epochs", default=300, type=int, help="max number of training epochs")
parser.add_argument("--batch_size", default=1, type=int, help="number of batch size")
parser.add_argument("--sw_batch_size", default=4, type=int, help="number of sliding window batch size")
parser.add_argument("--optim_lr", default=1e-4, type=float, help="optimization learning rate")
parser.add_argument("--optim_name", default="AdamW", type=str, help="optimization algorithm")
parser.add_argument("--reg_weight", default=1e-5, type=float, help="regularization weight")
parser.add_argument("--momentum", default=0.99, type=float, help="momentum")
parser.add_argument("--val_every", default=100, type=int, help="validation frequency")
parser.add_argument("--norm_name", default="instance", type=str, help="normalization name")
parser.add_argument("--workers", default=8, type=int, help="number of workers")
parser.add_argument("--feature_size", default=48, type=int, help="feature size")
parser.add_argument("--in_channels", default=4, type=int, help="number of input channels")
parser.add_argument("--out_channels", default=3, type=int, help="number of output channels")
parser.add_argument("--cache_dataset", action="store_true", help="use monai Dataset class")
parser.add_argument("--a_min", default=-1000.0, type=float, help="a_min in ScaleIntensityRanged")
parser.add_argument("--a_max", default=1560.0, type=float, help="a_max in ScaleIntensityRanged")
parser.add_argument("--b_min", default=0.0, type=float, help="b_min in ScaleIntensityRanged")
parser.add_argument("--b_max", default=1.0, type=float, help="b_max in ScaleIntensityRanged")
parser.add_argument("--roi_x", default=96, type=int, help="roi size in x direction")
parser.add_argument("--roi_y", default=96, type=int, help="roi size in y direction")
parser.add_argument("--roi_z", default=96, type=int, help="roi size in z direction")
parser.add_argument("--infer_overlap", default=0.5, type=float, help="sliding window inference overlap")
parser.add_argument("--lrschedule", default="warmup_cosine", type=str, help="type of learning rate scheduler")
parser.add_argument("--resume_ckpt", action="store_true", help="resume training from pretrained checkpoint")
parser.add_argument("--use_checkpoint", action="store_true", help="use gradient checkpointing to save memory")

def main():
    args=parser.parse_args()
    json_path = args.json_list
    logdir = args.logdir
    if not os.path.exists(logdir):
        os.makedirs(logdir)
    # Load Json & Append Root Path
    with open(json_path, 'r') as json_f:
        json_Data = json.load(json_f)
    data = json_Data["folds"]

    train_transforms = Compose(
        [
            LoadImaged(keys=["image", "oar_together", "tv_together","prior_knowledge","dose"]),
            EnsureChannelFirstd(keys=["image", "oar_together", "tv_together","prior_knowledge", "dose"]),
            ScaleIntensityRanged(
                keys=["image"], a_min=args.a_min, a_max=args.a_max,
                b_min=args.b_min, b_max=args.b_max, clip=True,
            ),
            Orientationd(keys=["image", "prior_knowledge", "tv_together", "oar_together", "dose"], axcodes="RAS"),
            SpatialPadd(keys=["image","tv_together","dose", "oar_together","prior_knowledge"], spatial_size = (args.roi_x, args.roi_y, args.roi_z)),
            
            
            RandCropByPosNegLabeld(
                keys=["image", "prior_knowledge", "tv_together","dose", "oar_together"],
                label_key="dose",
                spatial_size=(args.roi_x, args.roi_y, args.roi_z),
                neg=0,
                num_samples=1,
                allow_smaller=True
            ),
            Separate_structsd(keys=["oar_together"]),
            ToTensord(keys=["oar_split"]),
            ConcatItemsd(keys=['prior_knowledge','image',"tv_together", "oar_split"],name="input",dim=0),
            ToTensord(keys=["input","image","prior_knowledge","tv_together","dose"])
        ]
    )

    # standard PyTorch program style: create UNet, DiceLoss and Adam optimizer
    device = torch.device("cuda:0")
                    
    # Network parameters
    model_param = dict(
        base_num_filter=args.in_channels,
        num_blocks_per_stage_encoder=2,
        num_stages=4,
        pool_kernel_sizes=((2, 2, 2), (2, 2, 2), (2, 2, 2), (2, 2, 2)),
        conv_kernel_sizes=((3, 3, 3), (3, 3, 3), (3, 3, 3), (3, 3, 3)),
        conv_bottleneck_kernel_sizes=((3, 3, 3), (3, 3, 3), (3, 3, 3), (3, 3, 3)),
        num_blocks_per_stage_decoder=None,
        padding='same',
        num_steps_bottleneck=4,
        expansion_rate=1,
        pooling_type=MaxPool3d,
        pooling_kernel_size=(2, 2, 2),
        nonlin=ReLU
    )

    for f in range(args.start_fold, json_Data["nfolds"]):
        print("Fold",f)
        train_Data = data[f]['training']
        val_Data = data[f]['validation']

        train_ds = CacheDataset(data=train_Data, transform=train_transforms, num_workers=args.workers)
        train_loader = DataLoader(train_ds, batch_size=args.batch_size, shuffle=True, num_workers=args.workers)

        val_ds = CacheDataset(data=val_Data, transform=train_transforms, num_workers=args.workers)
        val_loader = DataLoader(val_ds, batch_size=args.batch_size, num_workers=args.workers)

        model = modular_hdunet(**model_param).to(device)
        loss_function = torch.nn.MSELoss()
        if args.optim_name == "Adam":
            optimizer = torch.optim.Adam(model.parameters(), lr=args.optim_lr, weight_decay=args.reg_weight)
        elif args.optim_name == "AdamW":
            optimizer = torch.optim.AdamW(model.parameters(), lr=args.optim_lr, weight_decay=args.reg_weight)
        elif args.optim_name == "SGD":
            optimizer = torch.optim.SGD(
                model.parameters(), lr=args.optim_lr, momentum=args.momentum, nesterov=True, weight_decay=args.reg_weight
            )
        else:
            raise ValueError("Unsupported Optimization Procedure: " + str(args.optim_name))
        val_loss_values = []
        best_val_loss = 1000.0
        epoch_loss_values = []
        for epoch in range(args.max_epochs):
            print("-" * 10)
            print(f"epoch {epoch + 1}/{args.max_epochs}")
            model.train()
            epoch_loss = 0
            step = 0
            for batch_data in train_loader:
                step += 1
                inputs, image, labels = (
                    batch_data["input"].to(device),
                    batch_data["image"].to(device),
                    batch_data["dose"].to(device),
                )
                optimizer.zero_grad()
                output_dose, output_ct = model(inputs)
                loss = loss_function(output_dose, labels)
                reconstruction_loss = loss_function(output_ct,image)
                total_loss = loss+reconstruction_loss
                total_loss.backward()
                optimizer.step()
                epoch_loss += total_loss.item()
                print(f"{step}/{len(train_ds) // train_loader.batch_size}, "
                    f"train_loss: {total_loss.item():.4f}")

                
            epoch_loss /= step
            epoch_loss_values.append(epoch_loss)

            print(f"epoch {epoch + 1} average loss: {epoch_loss:.4f}")
            if epoch % args.val_every == 0:
                    print('Entering Validation for epoch: {}'.format(epoch+1))
                    total_val_loss = 0
                    val_step = 0
                    model.eval()
                    for val_batch in val_loader:
                        val_step += 1
                        inputs, image, labels = (
                            val_batch["input"].to(device),
                            val_batch["image"].to(device),
                            val_batch["dose"].to(device),
                        )
                        output_dose, output_ct = model(inputs)
                        val_loss = loss_function(output_dose, labels)
                        ct_loss = loss_function(output_ct, image)
                        total_loss=val_loss+ct_loss
                        total_val_loss += total_loss.item()

                    total_val_loss /= val_step
                    val_loss_values.append(total_val_loss)

                    if total_val_loss < best_val_loss:
                        print(f"Saving new model based on validation loss {total_val_loss:.4f}")
                        best_val_loss = total_val_loss
                        checkpoint = {'epoch': args.max_epochs,
                                    'state_dict': model.state_dict(),
                                    'optimizer': optimizer.state_dict()
                                    }
                        torch.save(checkpoint, os.path.join(logdir, f'best_metric_model_{f}.pth'))
        model.cpu().detach()
        del model    

if __name__ == "__main__":
    main()
