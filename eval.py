import os, json, argparse
import torch
import copy
import numpy as np
import matplotlib.pyplot as plt
from monai.transforms import Compose, ScaleIntensityRanged
from monai.data import CacheDataset, DataLoader
from utils import *
from monai.transforms import (
    Compose,
    ScaleIntensityRanged,
    EnsureChannelFirstd,
    ToNumpyd,
    AsChannelLastd
)
from custom_transforms import Separate_structsd, LoadImaged
import torch.multiprocessing 
torch.multiprocessing.set_sharing_strategy('file_system')
parser = argparse.ArgumentParser(description="HDUNet for dose prediction pipeline")
parser.add_argument("--data_dir", default="/dataset/dataset0/", type=str, help="dataset directory")
parser.add_argument("--json_list", default="dataset_0.json", type=str, help="dataset json file")
parser.add_argument("--a_min", default=-1000, type=float, help="a_min in ScaleIntensityRanged")
parser.add_argument("--a_max", default=1560, type=float, help="a_max in ScaleIntensityRanged")
parser.add_argument("--b_min", default=0.0, type=float, help="b_min in ScaleIntensityRanged")
parser.add_argument("--b_max", default=1.0, type=float, help="b_max in ScaleIntensityRanged")
parser.add_argument("--workers", default=8, type=int, help="number of workers")
parser.add_argument("--output_dir", default="./outputs/", type=str, help="output directory",
)
def main():
    args = parser.parse_args()

    json_path = os.path.join(args.output_dir,'json_dataset_file.json')
    resultfolder = os.path.join(args.output_dir,'dvh')
    figure_folder = os.path.join(args.output_dir,'figures')
    if not os.path.exists(resultfolder):
        os.makedirs(resultfolder)
    if not os.path.exists(figure_folder):
        os.makedirs(figure_folder)
    struct_labels = np.load(os.path.join(args.data_dir,'trainliststruct_oar.npy'))[1:]
    tv_labels = np.load(os.path.join(args.data_dir,'trainliststruct_tv.npy'))
    tv_p = [70.0,54.25]
    print("struct_labels",struct_labels)
    print("tv_labels",tv_labels)
    print("tv_p",tv_p)

    ###------- DATA -------###
    with open(json_path, 'r') as json_f:
        json_Data = json.load(json_f)
    test_Data = json_Data['test']
    print(test_Data[0].keys())
    eval_transforms = Compose(
        [
            LoadImaged(keys=["image", "dose","pred_dose",'oar_together',"tv_together","pred_ct"]),
            EnsureChannelFirstd(keys=["image", "oar_together", "tv_together", "dose","pred_ct"]),
            ScaleIntensityRanged(
                keys=["image"], a_min=args.a_min, a_max=args.a_max,
                b_min=args.b_min, b_max=args.b_max, clip=True,
            ),
            Separate_structsd(keys=["oar_together"]),
            ToNumpyd(keys=["image","dose","pred_dose","pred_ct"]),
            ToNumpyd(keys=["oar_split"],dtype=bool),
            AsChannelLastd(keys=["oar_split"])
            #ToTensord(keys=["image","oar_split","tv_together","dose"])
        ]
    )

    # Define DataLoader using MONAI, CacheDataset needs to be used
    eval_ds = CacheDataset(data=test_Data, transform=eval_transforms, num_workers=args.workers)
    eval_loader = DataLoader(eval_ds, batch_size=1, shuffle=True, num_workers=args.workers)

    ###------- DVH DICT -------###
    def build_dvh_metrics_dict(struct_labels, target='CTV', prescription=["5425","7000"], metrics=['D2','D5', 'Dmean', 'D95', 'D99']):
        dvh_metrics = {}
        # Add OAR
        for s in struct_labels:
            dvh_metrics[s]={}
            for m in metrics:
                dvh_metrics[s][m]=[]
        # Add target
        for p in prescription:
            s = target+"_"+p
            dvh_metrics[s]={}
            for m in metrics:
                dvh_metrics[s][m]=[]
        return dvh_metrics

    dvh_metrics = build_dvh_metrics_dict(struct_labels = struct_labels) 
    dvh_metrics_gt=copy.deepcopy(dvh_metrics)
    dvh_metrics_pred=copy.deepcopy(dvh_metrics)
        
    for d in eval_loader:
        patient = d["patient_name"][0]
        print("patient name",patient)
        contours_exist = list(d["contours_exist"][0])[1:]
            
        ###------- FILL DVH DICT -------###
        # OAR
        for i in range(len(contours_exist)):
            if contours_exist[i] == 1:    
                dvh_gt, bins_gt = get_DVH(d["dose"][0,0,:,:,:].numpy(),d["oar_split"][0,:, :, :, i],800)
                dvh_pred, bins_pred = get_DVH(d["pred_dose"][0,:,:,:].numpy(),d["oar_split"][0,:, :, :, i],800)
                metrics = list(dvh_metrics_gt[struct_labels[i]].keys())
                metrics_values_gt = get_DVH_metrics(metrics,dvh_gt,bins_gt,d["dose"][0,0,:,:,:].numpy(),d["oar_split"][0,:, :, :, i])
                metrics_values_pred = get_DVH_metrics(metrics,dvh_pred,bins_pred,d["pred_dose"][0,:,:,:].numpy(),d["oar_split"][0,:, :, :, i])
                for j, m in enumerate(metrics):
                    dvh_metrics_gt[struct_labels[i]][m].append(metrics_values_gt[j]) 
                    dvh_metrics_pred[struct_labels[i]][m].append(metrics_values_pred[j]) 
        
        # TV
        for i in range(len(tv_labels)):
            # p = tv_labels[i].split('_')[1]
            p = tv_p[i]
            mask = np.where(d["tv_together"][0,:, :, :]==p,1,0)[0].astype(bool)
            dvh_gt, bins_gt = get_DVH(d["dose"][0,0,:,:,:].numpy(),mask,800)
            dvh_pred, bins_pred = get_DVH(d["pred_dose"][0,:,:,:].numpy(),mask,800)
            metrics = list(dvh_metrics_gt[tv_labels[i]].keys())
            metrics_values_gt = get_DVH_metrics(metrics,dvh_gt,bins_gt,d["dose"][0,0,:,:,:].numpy(),mask)
            metrics_values_pred = get_DVH_metrics(metrics,dvh_pred,bins_pred,d["pred_dose"][0,:,:,:].numpy(),mask)
            for j, m in enumerate(metrics):
                dvh_metrics_gt[tv_labels[i]][m].append(metrics_values_gt[j]) 
                dvh_metrics_pred[tv_labels[i]][m].append(metrics_values_pred[j]) 
            
        plt.figure("check", (12, 6))
        ax1 = plt.subplot(2,3, 1)
        plt.title("Dose")
        plt.axis('off')
        im = plt.imshow(d['dose'][0,0,:,:,60],vmin=0,vmax=80)
        plt.colorbar(im,cax=ax1)
        plt.subplot(2, 3, 2)
        plt.title("Pred Dose")
        plt.axis('off')
        im = plt.imshow(d['pred_dose'][0,:,:,60],vmin=0,vmax=80)
        plt.colorbar(im)
        plt.subplot(2, 3, 3)
        plt.title("Pred Dose - Dose")
        im = plt.imshow(d['pred_dose'][0,:,:,60]-d['dose'][0,0,:,:,60])

        plt.subplot(2,3, 4)
        plt.title("CT")
        plt.axis('off')
        im = plt.imshow(d['image'][0,0,:,:,60],vmin=0)
        plt.subplot(2, 3, 5)
        plt.title("Pred CT")
        plt.axis('off')
        im = plt.imshow(d['pred_ct'][0,0,:,:,60],vmin=0)
        plt.subplot(2, 3, 6)
        plt.title("Pred CT - CT")
        im = plt.imshow(d['pred_ct'][0,0,:,:,60]-d['image'][0,0,:,:,60])
        plt.savefig(os.path.join(figure_folder,patient+'_predictions.jpg'))

        compare_dvh_and_get_metrics(d['oar_split'][0], struct_labels, d['dose'][0][0], d['pred_dose'][0], metrics=['Dmean','D2'], savefile=os.path.join(resultfolder)+'/'+patient+'_')

    ###------- BOXPLOTS -------###
    plt.rcParams.update({'font.size': 22})
    metrics = ['D2','Dmean','Dmean','Dmean','Dmean','Dmean','Dmean','Dmean','Dmean','D2','Dmean','Dmean','D5']
    colors = ['deepskyblue','deepskyblue','deepskyblue','orange','orange','orange','orange','orange','orange','orange','orange','deepskyblue','deepskyblue']

    pred_boxplot = np.empty((len(eval_loader),len(struct_labels)))
    pred_boxplot[:,:] = np.NaN
    for i in range(len(struct_labels)):
        pred_boxplot[:len(dvh_metrics_gt[struct_labels[i]][metrics[i]]),i]=[(dvh_metrics_pred[struct_labels[i]][metrics[i]][j] - dvh_metrics_gt[struct_labels[i]][metrics[i]][j])/70*100 for j in range(len(dvh_metrics_gt[struct_labels[i]][metrics[i]]))]
    # Filter data using np.isnan
    mask = ~np.isnan(pred_boxplot)
    pred_boxplot = [d[m] for d, m in zip(pred_boxplot.T, mask.T)]
    #OAR
    plt.figure()
    position_pred_pk_oar = np.arange(1,len(struct_labels)+1)
    medianprops = dict(linewidth=2.5, color='k')
    box_pred_pk_oar = plt.boxplot(pred_boxplot,positions=position_pred_pk_oar,patch_artist=True, whis=[5, 95],sym='', medianprops=medianprops, showmeans=True)
    ax = plt.gca()
    for patch, color in zip(box_pred_pk_oar['boxes'], colors):
        patch.set_facecolor(color)
        patch.set_edgecolor('lightskyblue')
        if color == 'orange':
            patch.set(hatch='\\')
    # Boxplot PT
    plt.title('PT dose prediction error in DVH metrics for OARs')
    plt.xticks(np.arange(1,len(struct_labels)+0.5),struct_labels,rotation=45,ha='right')#'vertical')
    plt.ylabel('Predicted dose - Clinical dose \n[% of prescription for {}_HIGH]'.format('TV'), multialignment='center')
    plt.subplots_adjust(bottom=0.3)
    plt.grid(axis = 'y')
    plt.ylim(-5,5)
    plt.tight_layout()
    plt.savefig(os.path.join(figure_folder,"boxplot_oar.pdf"), format='pdf', dpi=1000)
    plt.savefig(os.path.join(figure_folder,"boxplot_oar.jpg"), format='jpg', dpi=1000)

    #TV
    metrics = ['D95','D99']

    target_labels_box = ["TV_LOW - D95", "TV_LOW - D99","TV_HIGH - D95", "TV_HIGH - D99"]#[target+str(int(p*100))+' - '+ m for p in prescription for m in metrics]
    pred_tv_boxplot = np.empty((len(eval_loader),len(target_labels_box)))
    pred_tv_boxplot[:,:] = np.NaN
    position_pred_tv = np.arange(len(target_labels_box))

    print(target_labels_box)
    for i in range(len(tv_labels)):
        for k in range(len(metrics)):
            pred_tv_boxplot[:len(dvh_metrics_gt[tv_labels[i]][metrics[k]]),i*len(tv_labels)+k]=[(dvh_metrics_pred[tv_labels[i]][metrics[k]][j] - dvh_metrics_gt[tv_labels[i]][metrics[k]][j])/70*100 for j in range(len(dvh_metrics_gt[tv_labels[i]][metrics[k]]))]
            
    colors = ['deepskyblue']*len(target_labels_box)
    print(colors)

    # Filter data using np.isnan
    mask = ~np.isnan(pred_tv_boxplot)
    pred_tv_boxplot = [d[m] for d, m in zip(pred_tv_boxplot.T, mask.T)]
    print('np.unique(pred_tv_boxplot)',np.unique(pred_tv_boxplot))
    plt.figure()

    medianprops = dict(linewidth=2.5, color='k')
    box_pred_tv = plt.boxplot(pred_tv_boxplot,positions=position_pred_tv,patch_artist=True, whis=[5, 95],sym='', medianprops=medianprops, showmeans=True)
    ax = plt.gca()

    for patch, color in zip(box_pred_tv['boxes'], colors):
        patch.set_facecolor('deepskyblue')

    plt.title('Error in DVH metrics for TVs')
    plt.xticks(np.arange(len(target_labels_box)),target_labels_box,rotation=45,ha='right')#'vertical')
    plt.ylabel('Predicted dose - Clinical dose \n[% of prescription for TV_HIGH]', multialignment='center')
    plt.subplots_adjust(bottom=0.3)
    plt.grid(axis = 'y')
    plt.ylim(-5,5)
    plt.tight_layout()
    plt.savefig(os.path.join(figure_folder,"boxplot_tv.pdf"), format='pdf', dpi=1000)
    plt.savefig(os.path.join(figure_folder,"boxplot_tv.jpg"), format='jpg', dpi=1000)

if __name__ == "__main__":
    main()