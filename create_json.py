import json
import os

path_nii = r'/CECI/proj/miro/HAN_PT_nii'
innerfolder = 'nii_files_3.0_3.0_3.0'
outlist = ["ANON104","ANON242","ANON373"]
patientlist = [patient for patient in os.listdir(path_nii) if os.path.isdir(os.path.join(path_nii,patient)) and patient not in outlist]

d = {
    "description": "han_3d_seg_prediction_flexibleUNET",
    "modality": {
        "0": "CT"
    },
    "name": "han_3d_seg",
    "numTest": 5,
    "numTraining": 47,
    "numValidation": 5,
    "numOut":3,
    "nfolds":11,
    "tensorImageSize": "3D",
    "folds":[],
    "out":[]}
for f in range(d['nfolds']):
    d['folds'].append({'training':[],'validation':[],'test':[]})
    for i, patient in enumerate(patientlist):
        if i<d['numTraining']:
            d['folds'][f]["training"].append({  'image':os.path.join(path_nii,patient,innerfolder,'ct.nii.gz'),
                                    'oar_together':os.path.join(path_nii,patient,innerfolder,'struct_oar.nii.gz'),
                                    'tv_together':os.path.join(path_nii,patient,innerfolder,'struct_tv.nii.gz'),
                                    'prior_knowledge':os.path.join(path_nii,patient,innerfolder,'prior_knowledge.nii.gz'),
                                    'dose':os.path.join(path_nii,patient,innerfolder,'dose.nii.gz'),
                                    'patient_name': patient})
        elif i< (d['numTraining']+d['numValidation']):
            d['folds'][f]["validation"].append({  'image':os.path.join(path_nii,patient,innerfolder,'ct.nii.gz'),
                                    'oar_together':os.path.join(path_nii,patient,innerfolder,'struct_oar.nii.gz'),
                                    'tv_together':os.path.join(path_nii,patient,innerfolder,'struct_tv.nii.gz'),
                                    'prior_knowledge':os.path.join(path_nii,patient,innerfolder,'prior_knowledge.nii.gz'),
                                    'dose':os.path.join(path_nii,patient,innerfolder,'dose.nii.gz'),
                                    'patient_name': patient})
        else: 
            d['folds'][f]["test"].append({  'image':os.path.join(path_nii,patient,innerfolder,'ct.nii.gz'),
                                    'oar_together':os.path.join(path_nii,patient,innerfolder,'struct_oar.nii.gz'),
                                    'tv_together':os.path.join(path_nii,patient,innerfolder,'struct_tv.nii.gz'),
                                    'prior_knowledge':os.path.join(path_nii,patient,innerfolder,'prior_knowledge.nii.gz'),
                                    'dose':os.path.join(path_nii,patient,innerfolder,'dose.nii.gz'),
                                    'patient_name': patient})
    patientlist=patientlist[-d["numTest"]:]+patientlist[:-d["numTest"]]
for patient in outlist:
    d['out'].append({  'image':os.path.join(path_nii,patient,innerfolder,'ct.nii.gz'),
                        'oar_together':os.path.join(path_nii,patient,innerfolder,'struct_oar.nii.gz'),
                        'tv_together':os.path.join(path_nii,patient,innerfolder,'struct_tv.nii.gz'),
                        'prior_knowledge':os.path.join(path_nii,patient,innerfolder,'prior_knowledge.nii.gz'),
                        'dose':os.path.join(path_nii,patient,innerfolder,'dose.nii.gz'),
                        'patient_name': patient})
with open('json_dataset_file.json', "w") as outfile:
    json.dump(d, outfile,indent=2)