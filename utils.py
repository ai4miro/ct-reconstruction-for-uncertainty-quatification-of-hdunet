import numpy as np
import os
import matplotlib.pyplot as plt
import pickle
# from keras import backend as K
import pandas as pd
import torch
import copy
# Definition of constants:
TRAINING=0
VALIDATION=1
TEST=2
TRAINING_FLIP=3

# ptv_value = [54.25, 70]
# # struct_labels = ['Brainstem', 'Esophagus_upper', 'GlotticArea', 'OralCavity', 'Parotid_L', 'Parotid_R', 'PharConsInf', 'PharConsMid', 'PharConsSup', 'SpinalCord', 'Submandibular_L', 'Submandibular_R', 'SupraglotLarynx']
# ptv_labels = ['PTV5425','PTV7000'] #["CTV5425","CTV7000"]
target = "TV" #To be chosen between "CTV" or "PTV"
# dvh_metrics= {'Brainstem':{'D2':[],'Dmax':[],'Dmean':[]}, 
#                 'SpinalCord':{'D2':[],'Dmax':[],'Dmean':[]},
#                 'GlotticArea':{'Dmean':[],'Dmax':[]},
#                 'OralCavity':{'Dmean':[],'Dmax':[]},
#                 'Parotid_R':{'Dmean':[],'Dmax':[]},
#                 'Parotid_L':{'Dmean':[],'Dmax':[]},
#                 'Submandibular_L':{'Dmean':[],'Dmax':[]},
#                 'Submandibular_R':{'Dmean':[],'Dmax':[]},
#                 'PharConsInf':{'Dmean':[],'Dmax':[]}, 
#                 'PharConsMid':{'Dmean':[],'Dmax':[]}, 
#                 'PharConsSup':{'Dmean':[],'Dmax':[]},
#                 'SupraglotLarynx':{'D5':[],'Dmax':[],'Dmean':[]},
#                 'Esophagus_upper':{'Dmean':[],'Dmax':[]},
#                 'PTV5425':{'D95':[],'D99':[],'Dmax':[],'Dmean':[]},
#                 'PTV7000':{'D95':[],'D99':[],'Dmax':[],'Dmean':[]}}

def mean_absolute_error(y_true, y_pred):
    """
    Computes the mean absolute error of Keras tensors. Used in test_model script.

    :param y_true: (3D tensor) containing the groundtruth dose. Shape: (img_rows, img_cols, img_slcs)  
    :param y_pred: (3D tensor) containing the prediction of the model. Shape: (img_rows, img_cols, img_slcs)
    :return: MAE metric 
    """
    return torch.mean(torch.abs(y_pred - y_true), axis=None)

def mean_squared_error(y_true, y_pred):
    """
    Computes the mean squared error of Keras tensors. Used in test_model script.

    :param y_true: (3D tensor) containing the groundtruth dose. Shape: (img_rows, img_cols, img_slcs)  
    :param y_pred: (3D tensor) containing the prediction of the model. Shape: (img_rows, img_cols, img_slcs)
    :return: MSE metric 
    """
    return torch.mean(torch.square(y_pred - y_true), axis=None)


def negloglik():
    def loss(y_true, y_pred):
        return -y_pred.log_prob(y_true)
    return loss

def params2name(params):
    """
    Returns name for output folder for corresponding parameters.

    :param params: (dictionary) containing parameters of the network and training experiment. 
    :return: (string) formatted with parameters in a readable way for the output folder name. 
    """
    results_name = ''
    for key in params.keys():
        # To deal with the loss function which is of type callable, other parameters go in the else.
        if (hasattr(params[key], '__call__')):
            p = params[key].__name__
        else:
            p = str(params[key])
        results_name = results_name + key + '_' + p +'_'
    # Remove last underscore before returning the string
    results_name = results_name[:-1]
    return results_name

def print_model_layers(model):
    """
    Print model layers: number, name, input shape and output shape.

    :param model: Keras model. 
    """
    for i, layer in enumerate(model.layers):
        print("Layer", i, "\t", layer.name, "\t\t", layer.input_shape, "\t", layer.output_shape) 

def save_learning_curve(dest_dir,hist, continue_training, additional_epochs=0):
    """
    Saves plot of training loss and val loss obtained from the history in the dest_dir.

    :param hist: (dictionary) containing the history of the model. Keys are train loss, val loss and metrics specified in the Keras compile function. 
    :param dest_dir: (string) Directory where to save the plot. 
    """
    if continue_training:
        hist1 = np.load(os.path.join(dest_dir,'history0150.npy'),allow_pickle=True).item()
        hist2 = np.load(os.path.join(dest_dir,'history%04d'%(additional_epochs)+'.npy'),allow_pickle=True).item()
        loss = hist1['loss']+hist2['loss']
        val_loss = hist1['val_loss']+hist2['val_loss']
        x = range(1, len(hist1['loss'])+len(hist2['loss'])+1)
        plt.figure()
        plt.plot(x, np.reshape(loss,(len(loss),)), 'o-', label='Training')
        plt.plot(x, np.reshape(val_loss,(len(val_loss),)), 'o-', label='Validation')
        plt.legend(loc='upper left')
        # plt.ylim((0,20))
        plt.ylabel('Loss')
        plt.grid(True)
        plt.savefig(dest_dir + '/learning_curves_extended.png')   
    else: 
        #hist = np.load(os.path.join(dest_dir,'history0150.npy'),allow_pickle=True).item()
        x = range(1, len(hist['loss'])+1)
        plt.figure()
        plt.plot(x, np.reshape(hist['loss'],(len(hist['loss']),)), 'o-', label='Training')
        plt.plot(x, np.reshape(hist['val_loss'],(len(hist['val_loss']),)), 'o-', label='Validation')
        plt.legend(loc='upper left')
        # plt.ylim((0,20))
        plt.ylabel('Loss')
        plt.grid(True)
        plt.savefig(dest_dir + '/learning_curves.png')


def save_mse_box_plot(mse, dest_dir, labels=None):
    """
    Saves mse box plot in the dest_dir.

    :param mse: (2D array) containing for every patient, the mse over every ROI. Shape: (nb_patients, nb_ROIs) 
    :param dest_dir: (string) Directory where to save the plot.
    :param labels: (2D array) containing the labels for each boxplot. Length: nb_ROIs
    """
    # Remove nan values (orelse, no boxplot is displayed)
    mask = ~np.isnan(mse)
    mse_array = [d[m] for d,m in zip(mse.T, mask.T)]
    print(len(mse_array))
    print(len(labels))
    plt.figure()
    plt.boxplot(mse_array,labels = labels)
    plt.xticks(rotation=45)
    plt.ylabel('Loss')
    plt.ylim((0,60))
    plt.grid(True)
    plt.title('MSE test set')
    plt.tight_layout()
    plt.savefig(dest_dir + '/box_plot.png')
    plt.show()

def save_mae_box_plot(mae, dest_dir, labels=None):
    """
    Saves mae box plot in the dest_dir.

    :param mae: (2D array) containing for every patient, the mae over every ROI. Shape: (nb_patients, nb_ROIs) 
    :param dest_dir: (string) Directory where to save the plot.
    :param labels: (2D array) containing the labels for each boxplot. Length: nb_ROIs
    """
    # Remove nan values (orelse, no boxplot is displayed)
    mask = ~np.isnan(mae)
    mae_array = [d[m] for d,m in zip(mae.T, mask.T)]

    plt.figure()
    plt.boxplot(mae_array,labels = labels)
    plt.xticks(rotation=45)
    plt.ylabel('Loss')
    plt.ylim((0,10))
    plt.grid(True)
    plt.title('MAE test set')
    plt.tight_layout()
    plt.savefig(dest_dir + '/box_plot_mae.png')
    plt.show()

def mse_per_roi(roi_splitted, y, y_pred):
    """
    Returns a 2D array with the mse values of each ROI of a patient.

    :param roi_splitted: (4D array)  of bool containing the binary mask of all ROIs. Shape: (img_rows, img_cls, img_slcs, nb_ROIs)
    :param y: (3D array) with groundtruth dose distribution. Shape: (img_rows, img_cls, img_slcs)
    :param y_pred: (3D array) with predicted dose distribution. Shape: (img_rows, img_cls, img_slcs)
    :return mse_roi: (2D array) containing mse for each ROI of y. Length: nb_ROIs
    """    
    # initialize metrics_values
    mse_roi = np.zeros((roi_splitted.shape[-1],1))
    # Fill with NaNs to be able to ignore them when plotting 
    mse_roi[:] = np.nan

    # Loop over structures and compute mse for each of them
    for s in range(roi_splitted.shape[-1]):
        # Check if structure is present
        if roi_splitted[:,:,:,s].max() > 0:
            mse_roi[s] =  mean_squared_error(y[roi_splitted[:,:,:,s]],y_pred[roi_splitted[:,:,:,s]])
    # Return array of mse over structures.
    return mse_roi

def mae_per_roi(roi_splitted, y, y_pred):
    """
    Returns a 2D array with the mae values of each ROI of a patient.

    :param roi_splitted: (4D array)  of bool containing the binary mask of all ROIs. Shape: (img_rows, img_cls, img_slcs, nb_ROIs)
    :param y: (3D array) with groundtruth dose distribution. Shape: (img_rows, img_cls, img_slcs)
    :param y_pred: (3D array) with predicted dose distribution. Shape: (img_rows, img_cls, img_slcs)
    :return mae_roi: (2D array) containing mae for each ROI of y. Length: nb_ROIs
    """    
    # initialize metrics_values
    mae_roi = np.zeros((roi_splitted.shape[-1],1))
    # Fill with NaNs to be able to ignore them when plotting 
    mae_roi[:] = np.nan

    # Loop over structures and compute mse for each of them
    for s in range(roi_splitted.shape[-1]):
        # Check if structure is present
        if roi_splitted[:,:,:,s].max() > 0:
            mae_roi[s] =  mean_absolute_error(y[roi_splitted[:,:,:,s]],y_pred[roi_splitted[:,:,:,s]])
    # Return array of mse over structures.
    return mae_roi

def sparse_vector_function(x, indices=None):
    """
    Convert a tensor into a dictionary of the non zero values and their corresponding indices

    :param x: the tensor or, if indices is not None, the values that belong at each index
    :param indices: the raveled indices of the tensor
    :return:  sparse vector in the form of a dictionary
    """
    if indices is None:
        y = {'data': x[x > 0], 'indices': np.nonzero(x.flatten())[-1]}
    else:
        y = {'data': x[x > 0], 'indices': indices[x > 0]}
    return y

def convert_csv(image, output_path, patient):
    """
    Save dose prediction in csv file, format for AAPM challenge 2020.

    :param image: (3D tensor) containing the dose prediction. Shape: (img_rows, img_cls, img_slcs)
    :param output_path: (string) directory where to save the csv file.
    :param patient: (string) corresponding patient.
    """ 
    image = np.squeeze(image)
    dose_to_save = sparse_vector_function(image, indices=None)
    dose_df = pd.DataFrame(data=dose_to_save['data'].squeeze(), index=dose_to_save['indices'].squeeze(), columns=['data'])
    dose_df.to_csv('{}/{}.csv'.format(output_path, patient))

def get_DVH_metrics(metrics,dvh,bins,y,struct_matrix_s):
    """
    get metrics from dvh and dose distributions
    :param metrics: list of str containing the metrics. E.g. ['D95','V5','Dmean,'Dmax']
    :param dvh: (1D vector) dvh y values 
    :param bins: (1D vector) dvh x values
    :param y: 3D matrix with dose distribution
    :param struct_matrix_s: 3D matrix  of bool containing the binary mask of structure s
    :return: metrics_values: (1D vector) metrics values for the structure s
    """
    # initialize metrics_values
    metrics_values = np.zeros((len(metrics,)))
    metrics_values[:] = np.nan
    for m in range(len(metrics)):
        # get metric
        if metrics[m][0] == 'D': # dose metric
            if metrics[m][1:].isnumeric(): 
                volume = float(metrics[m][1:])
                metrics_values[m] = np.interp(volume,np.flip(dvh,0),np.flip(bins[:-1],0))
            elif metrics[m][1:] == 'mean':
                metrics_values[m] = y[struct_matrix_s].mean()
            elif metrics[m][1:] == 'max':
                metrics_values[m] = y[struct_matrix_s].max()
        if metrics[m][0] == 'V': # volume metric
            dose = float(metrics[m][1:])
            metrics_values[m] = np.interp(dose,bins[:-1],dvh)                

    return metrics_values

def get_DVH(y,struct_matrix_s,num_bins):
    """
    get dvh 
    :param y: 3D matrix with dose distribution
    :param struct_matrix_s: 3D matrix  of bool containing the binary mask of structure s
    :param num_bins: number of bins to use for the histogram
    :return dvh: (1D vector) dvh y values
    :return bins: (1D vector) dvh x values
    """
    print('y.shape,struct_matrix_s.shape',y.shape,struct_matrix_s.shape)
    hist, bins = np.histogram(y[struct_matrix_s], bins=num_bins)
    dvh = (1. - np.cumsum(hist) / float(np.count_nonzero(struct_matrix_s)))*100
    
    return dvh,bins
def compare_3_dvh_and_get_metrics(struct_matrix, struct_labels, y, ypred1, ypred2, metrics, savefile):
    """
    plots dvhs from y and ypred and get dvh metrics 
    
    :param struct_matrix: 4D matrix  of bool containing the binary mask of all structures
    :param struct_labels: list of str with the labels for all structures
    :param y: 3D matrix with dose distribution
    :param ypred: 3D matrix with dose distribution
    :param metrics: list of str containing the metrics. E.g. ['D95','V5','Dmean,'Dmax']
    :param savefile: folder path to save the plots
    :return metrics_real: (2D vector) metric values for all structures for y
    :return metrics_pred: (2D vector) metric values for all structures for ypred
    """
    num_bins = 800
    fontsize = 14
    colorlabel=np.arange(struct_matrix.shape[-1])
    plt.rcParams["figure.figsize"] = [20,10]
    
    # initialize metrics_values
    metrics_real = np.zeros((struct_matrix.shape[-1],len(metrics)))
    metrics_real[:] = np.nan
    metrics_pred = np.zeros((struct_matrix.shape[-1],len(metrics)))
    metrics_pred[:] = np.nan 

    print("nb loops",struct_matrix.shape[-1])
    print("struct labels", len(struct_labels))
    plt.figure()
    
    # loop over structures for dose y
    for s in range(struct_matrix.shape[-1]):
        if struct_matrix[:,:,:,s].max() > 0:
            dvh,bins = get_DVH(y,struct_matrix[:,:,:,s],num_bins) 
            metrics_real[s,:] = get_DVH_metrics(metrics,dvh,bins,y,struct_matrix[:,:,:,s])
            
            plt.plot(bins[:-1], dvh, label=struct_labels[s], linewidth=4, color='C%1d'%colorlabel[s-1])
                    
    # loop over structures for dose ypred1
    for s in range(struct_matrix.shape[-1]):
        if struct_matrix[:,:,:,s].max() > 0:
            dvh,bins = get_DVH(ypred1,struct_matrix[:,:,:,s],num_bins) 
            metrics_pred[s,:] = get_DVH_metrics(metrics,dvh,bins,ypred1,struct_matrix[:,:,:,s])
            plt.plot(bins[:-1], dvh, linewidth=4, color='C%1d'%colorlabel[s-1],linestyle='dashed')
    
    # loop over structures for dose ypred2
    for s in range(struct_matrix.shape[-1]):
        if struct_matrix[:,:,:,s].max() > 0:
            dvh,bins = get_DVH(ypred2,struct_matrix[:,:,:,s],num_bins) 
            metrics_pred[s,:] = get_DVH_metrics(metrics,dvh,bins,ypred2,struct_matrix[:,:,:,s])
            plt.plot(bins[:-1], dvh, linewidth=4, color='C%1d'%colorlabel[s-1],linestyle='dotted')

    plt.legend(fancybox=True, framealpha=0.5, bbox_to_anchor=(1, 1), loc=2, fontsize=fontsize)
    plt.xlabel('Dose (Gy)', fontsize=fontsize)
    plt.ylabel('Volume (%)', fontsize=fontsize)   
    plt.title("DVH for prediction with prior knowledge of target objective functions (dashed line), \nmimicked dose (dotted line) and groundtruth (solid line)",multialignment='center')
    
    plt.gca().set_xlim(left=0., right=np.array([y.max(),ypred1.max()]).max())
    plt.gca().set_ylim(bottom=0., top=100)
    plt.grid()
    plt.tight_layout()
    plt.savefig(savefile)
    plt.close()
        
    return metrics_real, metrics_pred

def compare_dvh_and_get_metrics(struct_matrix, struct_labels, y, ypred, metrics, savefile):
    """
    plots dvhs from y and ypred and get dvh metrics 
    
    :param struct_matrix: 4D matrix  of bool containing the binary mask of all structures
    :param struct_labels: list of str with the labels for all structures
    :param y: 3D matrix with dose distribution
    :param ypred: 3D matrix with dose distribution
    :param metrics: list of str containing the metrics. E.g. ['D95','V5','Dmean,'Dmax']
    :param savefile: folder path to save the plots
    :return metrics_real: (2D vector) metric values for all structures for y
    :return metrics_pred: (2D vector) metric values for all structures for ypred
    """
    num_bins = 800
    fontsize = 14
    colorlabel=np.arange(struct_matrix.shape[-1])
    plt.rcParams["figure.figsize"] = [20,10]
    
    # initialize metrics_values
    metrics_real = np.zeros((struct_matrix.shape[-1],len(metrics)))
    metrics_real[:] = np.nan
    metrics_pred = np.zeros((struct_matrix.shape[-1],len(metrics)))
    metrics_pred[:] = np.nan 

    print("nb loops",struct_matrix.shape[-1])
    print("struct labels", len(struct_labels))
    plt.figure()
    
    # loop over structures for dose y
    for s in range(struct_matrix.shape[-1]):
        if struct_matrix[:,:,:,s].max() > 0:
            # print('struct_matrix[:,:,:,s].shape',struct_matrix[:,:,:,s].shape,type(struct_matrix[:,:,:,s]),struct_matrix[:,:,:,s].dtype, torch.unique(struct_matrix[:,:,:,s]))
            # print("y.shape",y.shape,type(y))
            dvh,bins = get_DVH(y,struct_matrix[:,:,:,s],num_bins) 
            metrics_real[s,:] = get_DVH_metrics(metrics,dvh,bins,y,struct_matrix[:,:,:,s])
            
            plt.plot(bins[:-1], dvh, label=struct_labels[s], linewidth=4, color='C%1d'%colorlabel[s-1])
                    
    # loop over structures for dose ypred
    for s in range(struct_matrix.shape[-1]):
        if struct_matrix[:,:,:,s].max() > 0:
            dvh,bins = get_DVH(ypred,struct_matrix[:,:,:,s],num_bins) 
            metrics_pred[s,:] = get_DVH_metrics(metrics,dvh,bins,ypred,struct_matrix[:,:,:,s])
            plt.plot(bins[:-1], dvh, linewidth=4, color='C%1d'%colorlabel[s-1],linestyle='dashed')
    print("savefile",savefile+'dvh.png')
    plt.legend(fancybox=True, framealpha=0.5, bbox_to_anchor=(1, 1), loc=2, fontsize=fontsize)
    plt.xlabel('Dose (Gy)', fontsize=fontsize)
    plt.ylabel('Volume (%)', fontsize=fontsize)   
    plt.title("DVH for prediction (dashed line) and groundtruth (solid line)")
    
    plt.gca().set_xlim(left=0., right=np.array([y.max(),ypred.max()]).max())
    plt.gca().set_ylim(bottom=0., top=100)
    plt.grid()
    plt.tight_layout()
    plt.savefig(savefile+'dvh.png')
    plt.close()
        
    return metrics_real, metrics_pred

# def get_dvh_dict(prediction_folder,datafolder, innerfolder, struct_labels):
#     nbins = 800
#     dvh_metrics_dict=copy.deepcopy(dvh_metrics)
#     for patient in os.listdir(prediction_folder):
#         patient_name=patient.split('.npy')[0]
#         dose = np.load(os.path.join(datafolder,patient_name,innerfolder,'dose.npy'))
#         struct = np.load(os.path.join(datafolder,patient_name,innerfolder,'struct.npy'))
#         struct_split = np.zeros((struct.shape[0],struct.shape[1],struct.shape[2],len(struct_labels)),dtype=bool)
#         contoursexist = np.load(os.path.join(datafolder,patient_name,innerfolder,'contoursexist.npy'))[1:] # Remove BODY structure, not relevant 
#         print(patient_name)
#         print(contoursexist)
#         for i in range(len(contoursexist)):
#             if contoursexist[i] == 1:    
#                 struct_split[:, :, :, i] = np.bitwise_and(struct, 2**(i+1)).astype(bool)
#                 dvh, bins = get_DVH(dose,struct_split[:, :, :, i],nbins)
#                 metrics = list(dvh_metrics_dict[struct_labels[i]].keys())
#                 metrics_values = get_DVH_metrics(metrics,dvh,bins,dose,struct_split[:, :, :, i])
#                 for j, m in enumerate(metrics):
#                     dvh_metrics_dict[struct_labels[i]][m].append(metrics_values[j]) 
        
#         ptv_together = np.load(os.path.join(datafolder,patient_name,innerfolder,'ptv.npy')) if os.path.exists(os.path.join(datafolder,patient_name,innerfolder,'ptv.npy')) else np.load(os.path.join(datafolder,patient_name,innerfolder,'ctv.npy'))
#         ptv_split = np.zeros((struct.shape[0],struct.shape[1],struct.shape[2],len(ptv_value)),dtype=bool)
#         unique_ptv = np.unique(ptv_together)
#         unique_ptv = unique_ptv[1:]
#         ptvsexist=np.zeros((2,))
#         j=0
#         for i in range(len(ptv_value)):
#             if ptv_value[i]==unique_ptv[j]: 
#                 ptv_split[:,:,:,i][ptv_together==ptv_value[i]]=1 #Binary masks
#                 dvh, bins = get_DVH(dose,ptv_split[:, :, :, i],nbins)
#                 metrics = list(dvh_metrics_dict[ptv_labels[i]].keys())
#                 metrics_values = get_DVH_metrics(metrics,dvh,bins,dose,ptv_split[:, :, :, i])
#                 for k, m in enumerate(metrics):
#                     dvh_metrics_dict[ptv_labels[i]][m].append(metrics_values[k]) 
#                 j+=1
def intersection(lst1, lst2): 
  
    # Use of hybrid method 
    temp = set(lst2) 
    lst3 = [value for value in lst1 if value in temp] 
    return lst3 

def get_dvh_dict_pred_outset(patient,k_max, predictionfolder,structfolder,innerfolder,struct_labels,dvh_metrics_dict):
    """
    predictionfolder: folder before cross-val
    structfolder: where we get the struct and contoursexist from
    datafolder: useless
    innerfolder: 'resized_3mm'
    """
    nbins = 800
    for k in range(1,k_max):#os.listdir(prediction_folder):
        #patient_name=patient.split('.npy')[0]
        dose = np.load(os.path.join(predictionfolder,'cross_val_k'+str(k),patient+'_norm_ctvp.npy'))
        # dose = np.load(os.path.join(datafolder,patient_name,innerfolder,'dose.npy'))
        struct = np.load(os.path.join(structfolder,patient,innerfolder,'struct.npy'))
        struct_split = np.zeros((struct.shape[0],struct.shape[1],struct.shape[2],len(struct_labels)),dtype=bool)
        contoursexist = np.load(os.path.join(structfolder,patient,innerfolder,'contoursexist.npy'))[1:] # Remove BODY structure, not relevant 
        for i in range(len(contoursexist)):
            if contoursexist[i] == 1:    
                struct_split[:, :, :, i] = np.bitwise_and(struct, 2**(i+1)).astype(bool)
                dvh, bins = get_DVH(dose,struct_split[:, :, :, i],nbins)
                metrics = list(dvh_metrics_dict[struct_labels[i]].keys())
                metrics_values = get_DVH_metrics(metrics,dvh,bins,dose,struct_split[:, :, :, i])
                for j, m in enumerate(metrics):
                    dvh_metrics_dict[struct_labels[i]][m].append(metrics_values[j]) 
        ptv_together = np.load(os.path.join(structfolder,patient,innerfolder,'ptv.npy')) if os.path.exists(os.path.join(structfolder,patient,innerfolder,'ptv.npy')) else  np.load(os.path.join(structfolder,patient,innerfolder,'ctv.npy'))
        #ptv_together = np.load(os.path.join(datafolder,patient_name,innerfolder,'ptv.npy'))
        unique_ptv = np.unique(ptv_together)
        print(unique_ptv)
        unique_ptv = unique_ptv[1:]
        ptv_split = np.zeros((struct.shape[0],struct.shape[1],struct.shape[2],len(unique_ptv)),dtype=bool)
        ptv_labels = ["TV"+str(unique_ptv[i]) for i in range(len(unique_ptv))]
        j=0
        for i in range(len(unique_ptv)):
            
            ptv_split[:,:,:,i][ptv_together==unique_ptv[i]]=1 #Binary masks
            dvh, bins = get_DVH(dose,ptv_split[:, :, :, i],nbins)
            metrics = list(dvh_metrics_dict[ptv_labels[i]].keys())
            metrics_values = get_DVH_metrics(metrics,dvh,bins,dose,ptv_split[:, :, :, i])
            for k, m in enumerate(metrics):
                dvh_metrics_dict[ptv_labels[i]][m].append(metrics_values[k]) 
            j+=1
    
    return dvh_metrics_dict

def get_dvh_dict_gt_outset(patientlist,predictionfolder,structfolder,innerfolder,struct_labels,dvh_metrics):
    """
    datafolder: where we get the dose from
    structfolder: where we get the struct and contoursexist from
    datafolder: useless
    innerfolder: 'resized_3mm'
    """
    nbins = 800
    dvh_metrics_dict=copy.deepcopy(dvh_metrics)
    for patient_name in patientlist:#os.listdir(prediction_folder):
        #patient_name=patient.split('.npy')[0]
        dose = np.load(os.path.join(predictionfolder,'cross_val_k'+str(1),patient_name+'_norm_ctvp_true.npy'))
        
        #dose = np.load(os.path.join(datafolder,patient_name,innerfolder,'dose.npy'))
        # dose = np.load(os.path.join(datafolder,patient_name,innerfolder,'dose.npy'))
        struct = np.load(os.path.join(structfolder,patient_name,innerfolder,'struct.npy'))
        struct_split = np.zeros((struct.shape[0],struct.shape[1],struct.shape[2],len(struct_labels)),dtype=bool)
        contoursexist = np.load(os.path.join(structfolder,patient_name,innerfolder,'contoursexist.npy'))[1:] # Remove BODY structure, not relevant 
        for i in range(len(contoursexist)):
            if contoursexist[i] == 1:    
                struct_split[:, :, :, i] = np.bitwise_and(struct, 2**(i+1)).astype(bool)
                dvh, bins = get_DVH(dose,struct_split[:, :, :, i],nbins)
                metrics = list(dvh_metrics_dict[struct_labels[i]].keys())
                metrics_values = get_DVH_metrics(metrics,dvh,bins,dose,struct_split[:, :, :, i])
                for j, m in enumerate(metrics):
                    dvh_metrics_dict[struct_labels[i]][m].append(metrics_values[j]) 

        ptv_together = np.load(os.path.join(structfolder,patient_name,innerfolder,'ptv.npy')) if os.path.exists(os.path.join(structfolder,patient_name,innerfolder,'ptv.npy')) else  np.load(os.path.join(structfolder,patient_name,innerfolder,'ctv.npy'))
        #ptv_together = np.load(os.path.join(datafolder,patient_name,innerfolder,'ptv.npy'))
        unique_ptv = np.unique(ptv_together)
        print(unique_ptv)
        unique_ptv = unique_ptv[1:]
        ptv_split = np.zeros((struct.shape[0],struct.shape[1],struct.shape[2],len(unique_ptv)),dtype=bool)
        ptv_labels = ["TV"+str(unique_ptv[i]) for i in range(len(unique_ptv))]
        j=0
        for i in range(len(unique_ptv)):
            
            ptv_split[:,:,:,i][ptv_together==unique_ptv[i]]=1 #Binary masks
            dvh, bins = get_DVH(dose,ptv_split[:, :, :, i],nbins)
            metrics = list(dvh_metrics_dict[ptv_labels[i]].keys())
            metrics_values = get_DVH_metrics(metrics,dvh,bins,dose,ptv_split[:, :, :, i])
            for k, m in enumerate(metrics):
                dvh_metrics_dict[ptv_labels[i]][m].append(metrics_values[k]) 
            j+=1
    return dvh_metrics_dict

#def get_dvh_dict_gt(patientlist,predictionfolder,structfolder,innerfolder,struct_labels):
def get_dvh_dict_gt(patientlist,predictionfolder,structfolder,innerfolder,struct_labels,dvh_metrics):
    """
    datafolder: where we get the dose from
    structfolder: where we get the struct and contoursexist from
    datafolder: useless
    innerfolder: 'resized_3mm'
    """
    nbins = 800
    dvh_metrics_dict=copy.deepcopy(dvh_metrics)
    for patient_name in patientlist:#os.listdir(prediction_folder):
        #patient_name=patient.split('.npy')[0]
        
        dose = np.load(os.path.join(predictionfolder,'true_npy_norm_ctvp',patient_name,'dose.npz'))['arr_0']
        #dose = np.load(os.path.join(datafolder,patient_name,innerfolder,'dose.npy'))
        # dose = np.load(os.path.join(datafolder,patient_name,innerfolder,'dose.npy'))
        struct = np.load(os.path.join(structfolder,patient_name,innerfolder,'struct.npz'))['arr_0']
        struct_split = np.zeros((struct.shape[0],struct.shape[1],struct.shape[2],len(struct_labels)),dtype=bool)
        contoursexist = np.load(os.path.join(structfolder,patient_name,innerfolder,'contoursexist.npz'))['arr_0'][1:] # Remove BODY structure, not relevant 
        for i in range(len(contoursexist)):
            if contoursexist[i] == 1:    
                struct_split[:, :, :, i] = np.bitwise_and(struct, 2**(i+1)).astype(bool)
                dvh, bins = get_DVH(dose,struct_split[:, :, :, i],nbins)
                metrics = list(dvh_metrics_dict[struct_labels[i]].keys())
                metrics_values = get_DVH_metrics(metrics,dvh,bins,dose,struct_split[:, :, :, i])
                for j, m in enumerate(metrics):
                    dvh_metrics_dict[struct_labels[i]][m].append(metrics_values[j]) 

        ptv_together = np.load(os.path.join(structfolder,patient_name,innerfolder,'ptv.npz'))['arr_0'] if os.path.exists(os.path.join(structfolder,patient_name,innerfolder,'ptv.npz')) else  np.load(os.path.join(structfolder,patient_name,innerfolder,'ctv.npz'))['arr_0']
        #ptv_together = np.load(os.path.join(datafolder,patient_name,innerfolder,'ptv.npy'))
        unique_ptv = np.unique(ptv_together)
        print(unique_ptv)
        unique_ptv = unique_ptv[1:]
        ptv_split = np.zeros((struct.shape[0],struct.shape[1],struct.shape[2],len(unique_ptv)),dtype=bool)
        ptv_labels = ["TV_LOW","TV_HIGH"]#+str(int(unique_ptv[i]*100)) for i in range(len(unique_ptv))]
        j=0
        for i in range(len(unique_ptv)):
            
            ptv_split[:,:,:,i][ptv_together==unique_ptv[i]]=1 #Binary masks
            dvh, bins = get_DVH(dose,ptv_split[:, :, :, i],nbins)
            metrics = list(dvh_metrics_dict[ptv_labels[i]].keys())
            metrics_values = get_DVH_metrics(metrics,dvh,bins,dose,ptv_split[:, :, :, i])
            for k, m in enumerate(metrics):
                dvh_metrics_dict[ptv_labels[i]][m].append(metrics_values[k]) 
            j+=1
    return dvh_metrics_dict

def get_dvh_dict_pred(patientlist,predictionfolder, structfolder,innerfolder,struct_labels,dvh_metrics):
    """
    predictionfolder: where we get the dose from
    structfolder: where we get the struct and contoursexist from
    datafolder: useless
    innerfolder: 'resized_3mm'

    """
    nbins = 800
    dvh_metrics_dict=copy.deepcopy(dvh_metrics)
    for patient_name in patientlist:# os.listdir(prediction_folder):
        #patient_name=patient.split('.npy')[0]
        dose = np.load(os.path.join(predictionfolder,'pred_npy_norm_ctvp',patient_name,'dose.npz'))['arr_0']
        struct = np.load(os.path.join(structfolder,patient_name,innerfolder,'struct.npz'))['arr_0']
        struct_split = np.zeros((struct.shape[0],struct.shape[1],struct.shape[2],len(struct_labels)),dtype=bool)
        contoursexist = np.load(os.path.join(structfolder,patient_name,innerfolder,'contoursexist.npz'))['arr_0'][1:] # Remove BODY structure, not relevant 
        for i in range(len(contoursexist)):
            if contoursexist[i] == 1:    
                struct_split[:, :, :, i] = np.bitwise_and(struct, 2**(i+1)).astype(bool)
                dvh, bins = get_DVH(dose,struct_split[:, :, :, i],nbins)
                metrics = list(dvh_metrics_dict[struct_labels[i]].keys())
                metrics_values = get_DVH_metrics(metrics,dvh,bins,dose,struct_split[:, :, :, i])
                for j, m in enumerate(metrics):
                    dvh_metrics_dict[struct_labels[i]][m].append(metrics_values[j]) 

        ptv_together = np.load(os.path.join(structfolder,patient_name,innerfolder,'ptv.npz'))['arr_0'] if os.path.exists(os.path.join(structfolder,patient_name,innerfolder,'ptv.npz')) else  np.load(os.path.join(structfolder,patient_name,innerfolder,'ctv.npz'))['arr_0']
        unique_ptv = np.unique(ptv_together)
        unique_ptv = unique_ptv[1:]
        ptv_split = np.zeros((struct.shape[0],struct.shape[1],struct.shape[2],len(unique_ptv)),dtype=bool)
        print(dvh_metrics_dict.keys())
        print(len(unique_ptv))
        ptv_labels = ["TV_LOW","TV_HIGH"]#+str(int(unique_ptv[i]*100)) for i in range(len(unique_ptv))]
        
        j=0
        for i in range(len(unique_ptv)):
            
            ptv_split[:,:,:,i][ptv_together==unique_ptv[i]]=1 #Binary masks
            dvh, bins = get_DVH(dose,ptv_split[:, :, :, i],nbins)
            metrics = list(dvh_metrics_dict[ptv_labels[i]].keys())
            metrics_values = get_DVH_metrics(metrics,dvh,bins,dose,ptv_split[:, :, :, i])
            for k, m in enumerate(metrics):
                dvh_metrics_dict[ptv_labels[i]][m].append(metrics_values[k]) 
            j+=1
    return dvh_metrics_dict


def get_dvh_dict_mim(patientlist, clinic_folder,innerfolder,struct_labels,dvh_metrics):
    """
    predictionfolder: where we get the dose from
    structfolder: where we get the struct and contoursexist from
    datafolder: useless
    innerfolder: 'resized_3mm'

    """
    nbins = 800
    dvh_metrics_dict=copy.deepcopy(dvh_metrics)
    for patient_name in patientlist:# os.listdir(prediction_folder):
        #patient_name=patient.split('.npy')[0]
        dose = np.load(os.path.join(clinic_folder,patient_name,innerfolder,'mimicked_dose.npz'))['arr_0']
        struct = np.load(os.path.join(clinic_folder,patient_name,innerfolder,'struct.npz'))['arr_0']
        struct_split = np.zeros((struct.shape[0],struct.shape[1],struct.shape[2],len(struct_labels)),dtype=bool)
        contoursexist = np.load(os.path.join(clinic_folder,patient_name,innerfolder,'contoursexist.npz'))['arr_0'][1:] # Remove BODY structure, not relevant 
        for i in range(len(contoursexist)):
            if contoursexist[i] == 1:    
                struct_split[:, :, :, i] = np.bitwise_and(struct, 2**(i+1)).astype(bool)
                dvh, bins = get_DVH(dose,struct_split[:, :, :, i],nbins)
                metrics = list(dvh_metrics_dict[struct_labels[i]].keys())
                metrics_values = get_DVH_metrics(metrics,dvh,bins,dose,struct_split[:, :, :, i])
                for j, m in enumerate(metrics):
                    dvh_metrics_dict[struct_labels[i]][m].append(metrics_values[j]) 

        ptv_together = np.load(os.path.join(clinic_folder,patient_name,innerfolder,'ptv.npz'))['arr_0'] if os.path.exists(os.path.join(clinic_folder,patient_name,innerfolder,'ptv.npz')) else  np.load(os.path.join(clinic_folder,patient_name,innerfolder,'ctv.npz'))['arr_0']
        unique_ptv = np.unique(ptv_together)
        unique_ptv = unique_ptv[1:]
        ptv_split = np.zeros((struct.shape[0],struct.shape[1],struct.shape[2],len(unique_ptv)),dtype=bool)
        print(dvh_metrics_dict.keys())
        print(len(unique_ptv))
        ptv_labels = ["TV_LOW","TV_HIGH"]#+str(int(unique_ptv[i]*100)) for i in range(len(unique_ptv))]
        
        j=0
        for i in range(len(unique_ptv)):
            
            ptv_split[:,:,:,i][ptv_together==unique_ptv[i]]=1 #Binary masks
            dvh, bins = get_DVH(dose,ptv_split[:, :, :, i],nbins)
            metrics = list(dvh_metrics_dict[ptv_labels[i]].keys())
            metrics_values = get_DVH_metrics(metrics,dvh,bins,dose,ptv_split[:, :, :, i])
            for k, m in enumerate(metrics):
                dvh_metrics_dict[ptv_labels[i]][m].append(metrics_values[k]) 
            j+=1
    return dvh_metrics_dict


def get_isodose_DICE(x,y,iso_dice,Dpre,crop):
    """
    gets dice similarity coefficient for isodoses volumes
    
    :param x: 3D matrix  of float containing dose distribution x
    :param y: 3D matrix  of float containing dose distribution y
    :param iso_dice: (int) maximum isodose to compute dice
    :param Dpre: (int) reference dose (=100% of the dose) in Gy
    :param crop: 3D matrix  of bool containing the region to crop 
                 or None if no crop is needed
    :return dice: 2D matrix with all dice for all isodoses
    """

    if crop is not None:
        x = x*crop
        y = y*crop

    #initialize dice
    dice = np.zeros((iso_dice,))    
    
    #get dice of isodoselines from 1% to iso_dice%
    for d in range(1,iso_dice):
        
        isodose_mask_x = np.zeros(x.shape)
        isodose_mask_x[(x>=Dpre*d/100).nonzero()] = 1 # set to one those values bigger than d% of 70Gy 
        # print(np.unique(isodose_mask_x))
        isodose_mask_y = np.zeros(y.shape)
        isodose_mask_y[(y>=Dpre*d/100).nonzero()] = 1 # set to one those values bigger than d% of 70Gy
        # print(np.unique(isodose_mask_y))

        intersection_xy = np.zeros(x.shape)
        intersection_xy = isodose_mask_x+isodose_mask_y
        intersection_xy[(intersection_xy<=1).nonzero()] = 0 # set the outsiders (values = 0 or 1) to 0
        intersection_xy[(intersection_xy>1).nonzero()] = 1 # set the intersection (values = 2) to 1
        # print(np.unique(intersection_xy))
        # avoid division by zero
        if np.sum((isodose_mask_y+isodose_mask_x)) == 0:
                    continue
        
        dice[d] = 2*(np.sum(intersection_xy))/np.sum((isodose_mask_x+isodose_mask_y).ravel())
        
    return dice
# outputfolder = os.path.join(r'../output_clean/no_data_augmentation','learn_rate_0.0003_loss_function_mse_final_number_of_pool_5_img_rows_96_img_cols_96_img_slcs_96_dense_enc_dec_False_instance_norm_True_dilate_bottleneck_True')

# resultfolder = os.path.join(outputfolder,'test_predictions')
# datafolder = r'../NAS_database/train_data/AAPM_challenge/train_pats'
# get_dvh_dict(os.path.join(resultfolder,'pred_npy'),datafolder)
# np.save(os.path.join(resultfolder,'dvh_metrics_dict.npy'),dvh_metrics_dict)
